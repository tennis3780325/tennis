class Tennis :
    def __init__(self, player) :
        self.player = player
        self.points = 0
        self.games = 0
        self.set = 0
        self.win = False
        
    def score(self) :
            self.points += 1
        
    def winTheGame(self, other) :
        if self.points >= 4 and self.points - other.points >= 2 :
            self.games += 1
            self.points = 0 
            other.points = 0
            
    def winTheSet(self, other) :
        if self.games >= 6 and self.games - other.games >= 2 :
            self.set += 1
            self.games = 0
            other.games = 0
             
    def winTheMatch(self) :
        if self.set == 2 :
            self.win = True
            
    def keepingScore(self, other) :
        self.score()
        self.winTheGame(other)
        self.winTheSet(other)
        self.winTheMatch()
        
    def __str__(self) :
        return f'Player {self.player} - Points: {self.points}, Games: {self.games}, Sets: {self.set}, Won: {self.win}'

def TennisMatch(n : str) :
    p = Tennis("A")
    q = Tennis("B")
    for i in n :
        if i == "A" :
            p.keepingScore(q)
        else :
            q.keepingScore(p)
    print(p)
    print(q)
print(TennisMatch("ABABABBBABABABABABBABABBBABABAABAABA"))
            
    
            
    
             
    

