points = {1: 15, 2: 30, 3: 40}

def point_classification(point: int) -> int:
        return points[point] if point in points else (point + 1) * 10


class Player:
    def __init__(self, player):
        self.player = player
        self.score = 0
        self.points = 0
        self.games = 0
        self.sets = 0
        self.won = False
        
        
    def scored(self):
        self.score += 1


    def __str__(self):
        if self.won == 1:
            return str(self.player + "has won")
        else:
            return f'Score: {point_classification(self.score)}; Games: {self.games}; Set: {self.sets}'
    

    def has_won_game(self, other):
        if self.score - other.score >= 2 and self.score >= 4:
            self.games += 1
            self.score = 0
            other.score = 0
        
        
    def has_won_set(self, other):
        if self.games - other.games >= 2 and self.game >= 6:
            self.set += 1
            self.games = 0
            other.games = 0
        
    def has_won_matches(self, other):
        if self.sets == 2:
            self.won = True


def Tennis(match_sequence: str) -> str:
    A = Player("A")
    B = Player("B")
    for point_to in match_sequence:
        if point_to == "A":
            A.scored()
        else:
            B.scored()
        A.has_won_game(B)
        B.has_won_game(A)
        A.has_won_set(B)
        B.has_won_set(A)
        A.has_won_matches(B)
        B.has_won_matches(A)
    
    return str(A), str(B)


print(Tennis("AABBAABBAA"))

        
    