def point_classification(point: int) -> int:
    points = {1: 15, 2: 30, 3: 40}
    return points[point] if point in points else (point + 1) * 10


def read_string(points: str):
    S = {'A': 0, 'B': 0}
    G = {'A': 0, 'B': 0}
    P = {'A': 0, 'B': 0}
    for player in points:
        if (P['A'] == 4 ^ P['B'] == 4) or abs(P['A'] - P['B']) == 2:
            P['A'] = 0
            P['B'] = 0
            G[player] += 1
            if G[player] >= 4 and abs(G['A'] - G['B']) > 2:
                S[player] += 1
                if S[player] == 2:
                    return player
        
    
        P[player] += 1
    P['A'] = point_classification(P['A'])
    P['B'] = point_classification(P['B'])
    return S, G, P
print(read_string("ABABBABBABABABBBAA"))



            
